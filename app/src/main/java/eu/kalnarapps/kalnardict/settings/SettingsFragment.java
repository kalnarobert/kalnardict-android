package eu.kalnarapps.kalnardict.settings;

/**
 * Created by kalnar on 24/04/17.
 */

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import eu.kalnarapps.kalnardict.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.app_preferences);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
