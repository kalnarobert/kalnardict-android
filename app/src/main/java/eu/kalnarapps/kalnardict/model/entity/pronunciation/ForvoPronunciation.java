package eu.kalnarapps.kalnardict.model.entity.pronunciation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForvoPronunciation {

    @SerializedName("attributes")
    @Expose
    private Attributes attributes;
    @SerializedName("items")
    @Expose
    private List<ForvoItem> items = null;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public List<ForvoItem> getItems() {
        return items;
    }

    public void setItems(List<ForvoItem> items) {
        this.items = items;
    }

}
