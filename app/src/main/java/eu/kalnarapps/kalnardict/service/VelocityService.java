package eu.kalnarapps.kalnardict.service;

import android.content.res.Resources;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by kalnar on 12/07/17.
 */

public class VelocityService {

    public static String translationHtmlFromTemplate(String translation, String cssFileName, Resources resources) throws IOException {
        return translationHtmlFromTemplate(translation, "template", cssFileName, resources);
    }

    private static String translationHtmlFromTemplate(String translation, String template, String cssFileName, Resources resources) throws IOException {
        Velocity.setProperty("resource.loader", "android");
        Velocity.setProperty("android.resource.loader.class", "eu.kalnarapps.kalnardict.service.velocity.AndroidResourceLoader");
        Velocity.setProperty("android.content.res.Resources", resources);
        Velocity.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogChute");
        Velocity.setProperty("packageName", "com.kalnar.apps.dictionarylayout");
        Velocity.init();
        Template t = Velocity.getTemplate(template);
        VelocityContext context = new VelocityContext();
        context.put("translationPlaceHolder", translation);
        context.put("cssName", cssFileName);
        StringWriter writer = new StringWriter();
        t.merge(context, writer);
        return writer.toString();
    }


}
