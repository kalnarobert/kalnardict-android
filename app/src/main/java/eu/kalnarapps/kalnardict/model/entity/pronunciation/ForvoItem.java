package eu.kalnarapps.kalnardict.model.entity.pronunciation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForvoItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("word")
    @Expose
    private String word;
    @SerializedName("original")
    @Expose
    private String original;
    @SerializedName("addtime")
    @Expose
    private String addtime;
    @SerializedName("hits")
    @Expose
    private Integer hits;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("langname")
    @Expose
    private String langname;
    @SerializedName("pathmp3")
    @Expose
    private String pathmp3;
    @SerializedName("pathogg")
    @Expose
    private String pathogg;
    @SerializedName("rate")
    @Expose
    private Integer rate;
    @SerializedName("num_votes")
    @Expose
    private Integer numVotes;
    @SerializedName("num_positive_votes")
    @Expose
    private Integer numPositiveVotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLangname() {
        return langname;
    }

    public void setLangname(String langname) {
        this.langname = langname;
    }

    public String getPathmp3() {
        return pathmp3;
    }

    public void setPathmp3(String pathmp3) {
        this.pathmp3 = pathmp3;
    }

    public String getPathogg() {
        return pathogg;
    }

    public void setPathogg(String pathogg) {
        this.pathogg = pathogg;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getNumVotes() {
        return numVotes;
    }

    public void setNumVotes(Integer numVotes) {
        this.numVotes = numVotes;
    }

    public Integer getNumPositiveVotes() {
        return numPositiveVotes;
    }

    public void setNumPositiveVotes(Integer numPositiveVotes) {
        this.numPositiveVotes = numPositiveVotes;
    }

}
