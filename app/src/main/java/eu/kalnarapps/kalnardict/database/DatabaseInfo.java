package eu.kalnarapps.kalnardict.database;

import android.provider.BaseColumns;

/**
 * Created by kalnar on 12/04/17.
 */

public class DatabaseInfo {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private DatabaseInfo() {
    }

    /* Inner class that defines the table contents */
    public static class DictionaryEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "word";
        public static final String COLUMN_NAME_SUBTITLE = "translation";
    }
}

