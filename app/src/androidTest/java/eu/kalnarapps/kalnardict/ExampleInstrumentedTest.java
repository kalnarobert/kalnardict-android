package eu.kalnarapps.kalnardict;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import eu.kalnarapps.kalnardict.model.entity.enums.Language;
import eu.kalnarapps.kalnardict.service.ForvoService;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.kalnar.apps.dictionarylayout", appContext.getPackageName());
    }

    @Test
    public void playForvoPronunciation() {
        Log.i("FORVO", ForvoService.getPronunciationPath("table", Language.FRENCH));
        System.out.println(ForvoService.getPronunciationPath("table", Language.FRENCH));
    }
}
