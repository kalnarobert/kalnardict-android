package eu.kalnarapps.kalnardict.service;

import android.os.Environment;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import eu.kalnarapps.kalnardict.R;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import eu.kalnarapps.kalnardict.MainActivity;
import eu.kalnarapps.kalnardict.model.entity.Language;
import eu.kalnarapps.kalnardict.model.entity.pronunciation.ForvoApiEndPoints;
import eu.kalnarapps.kalnardict.model.entity.pronunciation.ForvoItem;
import eu.kalnarapps.kalnardict.model.entity.pronunciation.ForvoPronunciation;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForvoService {

    private static final String FILE_DIR = "kalnarapps/kalnar-dict/audio";
    private static final String FORVO_TAG = "FORVO";

    public static String getPronunciationPath(String word, Language language) {

        final String FORVO_KEY = PreferenceManager.getDefaultSharedPreferences(MainActivity.getAppContext()).getString(MainActivity.getAppContext().getResources().getString(R.string.forvo_key), "");

        String languageCode = language.getLangCode();
        String dirPath = Environment.getExternalStorageDirectory() + File.separator + FILE_DIR + File.separator + language.getLangCode() + File.separator;
        File audioDirectory = new File(dirPath);
        if (!audioDirectory.exists()) {
            audioDirectory.mkdirs();
        }
        String pathname = audioDirectory.getAbsolutePath() + "/" + word + ".mp3";
        if (new File(pathname).exists()) {
            FileInputStream fis;
            Log.d(FORVO_TAG, "procceding to playing already downloaded file: " + pathname);
            return pathname;
        }

        final String BASE_URL = "https://apifree.forvo.com/";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        ForvoApiEndPoints apiService = retrofit.create(ForvoApiEndPoints.class);
        Call<ForvoPronunciation> call = apiService.getWord(word, FORVO_KEY, languageCode);
        try {
            Response<ForvoPronunciation> response = call.execute();
            System.out.println(response.code());
            List<ForvoItem> items = response.body().getItems();
            if (items.size() == 0) {
                pathname = null;
            }
            for (ForvoItem item : items) {
                if (item.getPathmp3() != null) {
                    FileUtils.copyURLToFile(new URL(item.getPathmp3()), new File(pathname));
                }
                Log.d(FORVO_TAG, "procceding to playing just downloaded file: " + pathname);
                return pathname;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("no available mp3");
            e.printStackTrace();
        }
        return pathname;
    }

}
