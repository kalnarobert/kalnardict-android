package eu.kalnarapps.kalnardict.model.entity.pronunciation;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ForvoApiEndPoints {
    // Request method and URL specified in the annotation
    @GET("action/word-pronunciations/format/json/word/{word}/limit/1/key/{key}/language/{languageCode}")
    Call<ForvoPronunciation> getWord(@Path("word") String word, @Path("key") String key, @Path("languageCode") String languageCode);
}