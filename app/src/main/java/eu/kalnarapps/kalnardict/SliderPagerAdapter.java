package eu.kalnarapps.kalnardict;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;

import java.util.Objects;

import eu.kalnarapps.kalnardict.model.entity.LanguageService;
import eu.kalnarapps.kalnardict.service.AndroidSystemTools;

/**
 * Created by kalnar on 10/04/17.
 */

public class SliderPagerAdapter extends FragmentPagerAdapter {

    private static int PAGER_COUNT;
    private final FragmentActivity context;

    SliderPagerAdapter(FragmentManager fragmentManager, FragmentActivity mainActivity) {
        super(fragmentManager);
        PAGER_COUNT = LanguageService.getPairs(mainActivity).size();
        context = mainActivity;
    }

    @Override
    public int getCount() {
        return PAGER_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        SliderFragment fragment = SliderFragment.create(LanguageService.getPairs(context).get(position));
        fragment.setOnItemSelectedListener((parent, view, pos, id) -> {
            FragmentTransaction transaction = fragment.getChildFragmentManager().beginTransaction();
            TranslationFragment newFragment = TranslationFragment.createFromWordWithIdAndLang(id, position, fragment.getLangId());
            newFragment.beforeDestroyed(() -> {
                fragment.focusEditTextAndShowKeyboard();
                fragment.updateListView();
            });
            newFragment.setOnNewSearchSelectedListener(() -> {
                Objects.requireNonNull(fragment.getActivity()).onBackPressed();
                fragment.focusEditTextAndShowKeyboardAndClearText();
            });
            transaction.replace(R.id.fragment_main_container, newFragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();
            AndroidSystemTools.hideSoftKeyboard(view);
        });
        return fragment;
    }

}
