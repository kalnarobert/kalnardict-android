package eu.kalnarapps.kalnardict;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import eu.kalnarapps.kalnardict.database.DictionaryDbHelper;
import eu.kalnarapps.kalnardict.model.entity.LanguagePair;
import eu.kalnarapps.kalnardict.model.entity.LanguageService;

/**
 * Created by kalnar on 05/09/17.
 */

public class DeleteFavoritesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delete_favorites);

        LinearLayout layout = (LinearLayout) findViewById(R.id.delete_favorites_layout);

        for (LanguagePair languagePair : LanguageService.getPairs(getApplicationContext())) {
            Button button = new Button(this);
            button.setText("delete favorites in " + languagePair.getId().toLowerCase());
            button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            button.setOnClickListener(view -> {
                DictionaryDbHelper dbHelper = DictionaryDbHelper.getInstance(this);
                dbHelper.removeFavorites(languagePair);
            });
            layout.addView(button);
        }
    }
}
