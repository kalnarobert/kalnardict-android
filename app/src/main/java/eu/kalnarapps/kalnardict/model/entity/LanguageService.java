package eu.kalnarapps.kalnardict.model.entity;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import eu.kalnarapps.kalnardict.database.DictionaryDbHelper;

import static eu.kalnarapps.kalnardict.TranslationFragment.PRIMARY_LANG_ID;

public class LanguageService {

    private static List<LanguagePair> languagePairs = new ArrayList<>();

    public static List<LanguagePair> getPairs(Context context) {
        if (languagePairs.isEmpty()) {
            readDictionaryTables(context);
        }
        return languagePairs;
    }

    private static void readDictionaryTables(Context context) {

        DictionaryDbHelper dbHelper = DictionaryDbHelper.getInstance(context);
        List<String> pairIdList = dbHelper.listDictionaryTables();
        for (String pairId : pairIdList) {
            languagePairs.add(new LanguagePair(pairId, context));
        }
    }

    public static String getHint(LanguagePair languagePair, String langId) {
        if (languagePair.getSecondaryLanguage() != null) {
            String firstLang = languagePair.getPrimaryLanguage().getHint();
            String arrow = " -> ";
            String secondLang = languagePair.getSecondaryLanguage().getHint();
            if (langId.equals(PRIMARY_LANG_ID)) {
                return firstLang + arrow + secondLang;
            } else {
                return secondLang + arrow + firstLang;
            }
        } else {
            return languagePair.getPrimaryLanguage().getHint();
        }
    }
}
