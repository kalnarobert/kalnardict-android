package eu.kalnarapps.kalnardict.model.entity;

import java.util.Locale;

public class Language {

    private final String hint;
    private String langCode;
    private String cssName;

    public Language(String langCode, String cssName) {
        this.langCode = langCode;
        if (isIso(this.langCode)) {
            Locale loc = new Locale(this.langCode);
            this.hint = loc.getDisplayLanguage(loc);
        } else {
            this.hint = this.langCode;
        }
        this.cssName = cssName;
    }

    public boolean isIso(String languageCode) {
        for (String iso : Locale.getISOLanguages()) {
            if (iso.equals(languageCode)) {
                return true;
            }
        }
        return false;
    }

    public String getLangCode() {
        return langCode;
    }

    public String getCssName() {
        return cssName;
    }

    public String getHint() {
        return hint;
    }
}
