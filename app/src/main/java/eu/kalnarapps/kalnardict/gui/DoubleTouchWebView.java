package eu.kalnarapps.kalnardict.gui;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.webkit.WebView;

import static eu.kalnarapps.kalnardict.gui.WebViewInterface.ButtonPressedListener;
import static eu.kalnarapps.kalnardict.gui.WebViewInterface.Edge;
import static eu.kalnarapps.kalnardict.gui.WebViewInterface.FavoriteStateChecker;

/**
 * Created by kalnar on 08/07/17.
 */

public class DoubleTouchWebView extends WebView implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    public static final String FLING_TAG = "fling";
    public static final String FAV_TAG = "fav";
    public static final String EDGE = "edge";
    private GestureDetectorCompat mDetector;
    private OnDoubleTapAction onDoubleTap;
    private ScrollState scrollstate = ScrollState.WAIT;
    private OnDownAfterReachedBottom onDownAfterReachedBottom;
    private OnUpAfterReachedTop onUpAfterReachedTop;
    private OnTwoFingerSwipeDown onTwoFingerSwipeDown;
    private OnTwoFingerSwipeUp onTwoFingerSwipeUp;
    private WebViewInterface webViewInterface;
    private int fingerCount = 0;
    private boolean firstTwoFingerEventToBeCaled = false;

    public DoubleTouchWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mDetector = new GestureDetectorCompat(getContext(), this);
        mDetector.setOnDoubleTapListener(this);
        webViewInterface = new WebViewInterface(context);
        webViewInterface.addEdgeReachedListener(this::reachedEdge);
        addJavascriptInterface(webViewInterface, "Android");
    }

    public DoubleTouchWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mDetector = new GestureDetectorCompat(getContext(), this);
        mDetector.setOnDoubleTapListener(this);
    }

    public DoubleTouchWebView(Context context) {
        super(context);
        mDetector = new GestureDetectorCompat(getContext(), this);
        mDetector.setOnDoubleTapListener(this);
    }

    private void reachedEdge(Edge edge) {
        Log.d(EDGE, edge.name());
        switch (edge) {
            case BOTTOM:
                scrollstate = ScrollState.BOTTOM;
                break;
            case TOP:
                scrollstate = ScrollState.TOP;
                break;
            case TOP_AND_BOTTOM:
                scrollstate = ScrollState.SMALLPAGE;
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (MotionEvent.ACTION_DOWN == action || MotionEvent.ACTION_POINTER_DOWN == action) {
            if (fingerCount == 1) {
                firstTwoFingerEventToBeCaled = true;
            }
            fingerCount++;
        } else if (MotionEvent.ACTION_UP == action || MotionEvent.ACTION_POINTER_UP == action) {
            if (fingerCount == 2) {
                firstTwoFingerEventToBeCaled = false;
            }
            fingerCount--;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        onDoubleTap.action();
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }


    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if ((e1.getPointerCount() == 2 || e2.getPointerCount() == 2) && firstTwoFingerEventToBeCaled) {
            if (distanceY < 0) {
                onTwoFingerSwipeUp.action();
            } else {
                onTwoFingerSwipeDown.action();
            }
            firstTwoFingerEventToBeCaled = false;
        }
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }


    public void setOnDoubleTap(OnDoubleTapAction onDoubleTap) {
        this.onDoubleTap = onDoubleTap;
    }

    @Override
    protected void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
    }

    @Override
    public void loadDataWithBaseURL(String baseUrl, String data, String mimeType, String encoding, String historyUrl) {
        super.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
    }

    public void setOnDownAfterReachedBottom(OnDownAfterReachedBottom onDownAfterReachedBottom) {
        this.onDownAfterReachedBottom = onDownAfterReachedBottom;
    }

    public void addButtonPressedListener(ButtonPressedListener buttonPressedListener) {
        webViewInterface.addButtonPressedListener(buttonPressedListener);
    }

    public void setFavoriteStateChecker(FavoriteStateChecker favoriteStateChecker) {
        webViewInterface.setFavoriteStateChecker(favoriteStateChecker);
    }

    public void setOnUpAfterReachedTop(OnUpAfterReachedTop onUpAfterReachedTop) {
        this.onUpAfterReachedTop = onUpAfterReachedTop;
    }

    public void setOnTwoFingerSwipeUp(OnTwoFingerSwipeUp onTwoFingerSwipeUp) {
        this.onTwoFingerSwipeUp = onTwoFingerSwipeUp;
    }

    public void setOnTwoFingerSwipeDown(OnTwoFingerSwipeDown onTwoFingerSwipeDown) {
        this.onTwoFingerSwipeDown = onTwoFingerSwipeDown;
    }

    public enum ScrollState {
        BOTTOM, TOP, RIGHT, LEFT, SCROLLING, WAIT, SMALLPAGE
    }

    public interface OnDoubleTapAction {
        void action();
    }

    public interface OnDownAfterReachedBottom {
        void action();
    }

    public interface OnUpAfterReachedTop {
        void action();
    }

    public interface OnTwoFingerSwipeUp {
        void action();
    }

    public interface OnTwoFingerSwipeDown {
        void action();
    }
}
