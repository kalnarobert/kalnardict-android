
# kalnarDict

kalnarDict is a dictionary app that uses a local SQLite database to lookup dictionaries.

Dictionaries from FreeDict project can be used to create databases. 

To create a database, you can use
[tei_to_kalnardict_db](https://gitlab.com/kalnarobert/tei_to_kalnadict_db).
This converts tei format
[FreeDict](https://github.com/freedict/fd-dictionaries) to needed SQLite
database structure.

## Database structure

Documentation for database structure coming soon..


