package eu.kalnarapps.kalnardict;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.kalnarapps.kalnardict.database.DictionaryEntity;
import eu.kalnarapps.kalnardict.database.DictionaryDbHelper;
import eu.kalnarapps.kalnardict.gui.DoubleTouchWebView;
import eu.kalnarapps.kalnardict.gui.WebViewInterface;
import eu.kalnarapps.kalnardict.model.entity.Language;
import eu.kalnarapps.kalnardict.model.entity.LanguagePair;
import eu.kalnarapps.kalnardict.model.entity.LanguageService;
import eu.kalnarapps.kalnardict.service.ForvoService;
import eu.kalnarapps.kalnardict.service.VelocityService;

import static eu.kalnarapps.kalnardict.model.entity.LanguagePair.CSS_DIR;

/**
 * Created by kalnar on 12/04/17.
 */

public class TranslationFragment extends Fragment {

    public static final String PRIMARY_LANG_ID = "1";
    public static final String SECONDARY_LANG_ID = "2";
    private static final String WORD_ID = "word_id";
    private static final String STYLE_CSS = "css_name";
    private static final String STYLE_CSS_ALT = "css_name_alt";
    private static final String LANG_DIRECTION_ID = "lang_direction_id";
    private static final String TRANS_TAG = "trans_tag";
    private static final String LANG_PAIR = "language_pair_tag";
    @BindView(R.id.translation_webview)
    protected DoubleTouchWebView transWebView;
    @BindView(R.id.clearButton)
    protected Button button;
    @BindView(R.id.newSearchButton)
    protected Button newSearchButton;
    private DictionaryDbHelper dbHelper;
    private OnNewSearchSelectedListener onNewSearchSelectedListener;
    private BeforeDestroyListener beforeDestroyed;
    private LanguagePair languagePair;
    private long id;

    public TranslationFragment() {
    }

    public static TranslationFragment createFromWordWithIdAndLang(long id, int pagerPos, String langId) {
        TranslationFragment translationFragment = new TranslationFragment();
        LanguagePair languagePair = LanguageService.getPairs(translationFragment.getContext()).get(pagerPos);
        Language lang = langId.equals(PRIMARY_LANG_ID) ? languagePair.getPrimaryLanguage() : languagePair.getSecondaryLanguage();
        Language langAlt = langId.equals(SECONDARY_LANG_ID) ? languagePair.getPrimaryLanguage() : languagePair.getSecondaryLanguage();
        Bundle bundle = new Bundle();
        bundle.putLong(WORD_ID, id);
        translationFragment.setId(id);
        translationFragment.languagePair = languagePair;
        bundle.putString(STYLE_CSS, lang.getCssName());
        bundle.putString(STYLE_CSS_ALT, langAlt.getCssName());
        bundle.putString(LANG_DIRECTION_ID, langId);
        translationFragment.setArguments(bundle);
        return translationFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.translation_layout, container, false);
        ButterKnife.bind(this, rootView);
        rootView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        installClearFunction();

        String cssFileName = Environment.getExternalStorageDirectory() + File.separator + CSS_DIR + File.separator;
        if (getArguments().getString(LANG_DIRECTION_ID).equals(PRIMARY_LANG_ID)) {
            cssFileName += languagePair.getPrimaryLanguage().getCssName();
        } else {
            cssFileName += languagePair.getSecondaryLanguage().getCssName();
        }
        dbHelper = DictionaryDbHelper.getInstance(getContext());
        DictionaryEntity dictionaryEntity = dbHelper.getEntity((int) getArguments().getLong(WORD_ID), languagePair);
        String translation = dictionaryEntity.getTranslation();
        String htmlData = null;
        try {
            htmlData = VelocityService.translationHtmlFromTemplate(translation, cssFileName, getResources());
            Log.i("velocity", "html data loaded into string");
        } catch (IOException e) {
            Log.i("velocity", "css: " + cssFileName + ", dunno");
            e.printStackTrace();
        }
        transWebView.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "charset=utf-8", "UTF-8");
        transWebView.setOnDoubleTap(onNewSearchSelectedListener::onNewSearchSelected);
        transWebView.setOnTwoFingerSwipeDown(this::loadNextFavorites);
        transWebView.setOnTwoFingerSwipeUp(this::loadPreviousFavorites);
        transWebView.getSettings().setBuiltInZoomControls(true);
        transWebView.getSettings().setDisplayZoomControls(false);
        transWebView.getSettings().setJavaScriptEnabled(true);
        transWebView.addButtonPressedListener(this::handleHtmlButtons);
        transWebView.setFavoriteStateChecker(() -> dbHelper.isEntityFavorite(id, languagePair));
        return rootView;
    }

    private void handleHtmlButtons(WebViewInterface.WebButton webButton) {
        Log.d("TRANS", "webbutton pressed: " + webButton.name());
        switch (webButton) {
            case FAVORITE:
                if (dbHelper.isEntityFavorite(id, languagePair)) {
                    long idToBeRemoved = id;
                    if (dbHelper.isNotLastFavorite(id, languagePair)) {
                        transWebView.post(this::loadNextFavorites);
                        setId((long) dbHelper.getNextEntityFromFavorites(id, languagePair).getId());
                    }
                    dbHelper.toggleFavoriteState(idToBeRemoved, languagePair);
//                    getFragmentManager().popBackStack();
                } else {
                    dbHelper.toggleFavoriteState(id, languagePair);
                }
                break;
            case FAVORITE_NEXT:
                transWebView.post(this::loadNextFavorites);
                break;
            case FAVORITE_PREVIOUS:
                transWebView.post(this::loadPreviousFavorites);
                break;
            case FORVO:
                String audioFilePath = ForvoService.getPronunciationPath(DictionaryEntity.getCURRENT().getWord(), getLanguage());
                if (audioFilePath != null) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), Uri.parse(audioFilePath));
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(MediaPlayer::release);
                }
            default:
                break;
        }
    }

    private void loadPreviousFavorites() {
        loadFavorite(dbHelper.getPreviousEntityFromFavorites(id, languagePair));
    }

    private void loadNextFavorites() {
        loadFavorite(dbHelper.getNextEntityFromFavorites(id, languagePair));
    }

    private void loadFavorite(DictionaryEntity favorite) {
        String newHtml = null;
        if (favorite == null) {
            return;
        }
        id = favorite.getId();
        try {
            newHtml = VelocityService.translationHtmlFromTemplate(favorite.getTranslation(), getLanguage().getCssName(), getResources());
        } catch (IOException e) {
            e.printStackTrace();
        }
        String finalNewHtml = newHtml;
        transWebView.loadDataWithBaseURL("file:///android_asset/", finalNewHtml, "text/html", "UTF-8", null);
    }

    private Language getLanguage() {
        String langId = getArguments().getString(LANG_DIRECTION_ID);
        Language lang = langId.equals(PRIMARY_LANG_ID) ? languagePair.getPrimaryLanguage() : languagePair.getSecondaryLanguage();
        return lang;
    }


    private void installClearFunction() {
        button.setOnClickListener(v -> getActivity().onBackPressed());
        newSearchButton.setOnClickListener(v -> onNewSearchSelected());
    }

    @Override
    public void onDestroy() {
        if (dbHelper != null) {
            dbHelper.close();
        }
        beforeDestroyed.beforeDestroyed();
        super.onDestroy();
    }


    public void onNewSearchSelected() {
        onNewSearchSelectedListener.onNewSearchSelected();
    }

    public void setOnNewSearchSelectedListener(OnNewSearchSelectedListener onNewSearchSelectedListener) {
        this.onNewSearchSelectedListener = onNewSearchSelectedListener;
    }

    public void beforeDestroyed(BeforeDestroyListener beforeDestroyed) {
        this.beforeDestroyed = beforeDestroyed;
    }

    public void setId(Long id) {
        this.id = (int) (long) id;
    }

    interface OnNewSearchSelectedListener {
        void onNewSearchSelected();
    }

    interface BeforeDestroyListener {
        void beforeDestroyed();
    }

}
