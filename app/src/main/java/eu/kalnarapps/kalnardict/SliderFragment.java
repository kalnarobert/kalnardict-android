package eu.kalnarapps.kalnardict;

import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.kalnarapps.kalnardict.database.DictionaryDbHelper;
import eu.kalnarapps.kalnardict.database.CursorAdapter;
import eu.kalnarapps.kalnardict.model.entity.LanguagePair;
import eu.kalnarapps.kalnardict.model.entity.LanguageService;

import static eu.kalnarapps.kalnardict.SliderFragment.Mode.FAVORITES;
import static eu.kalnarapps.kalnardict.SliderFragment.Mode.SEARCH;
import static eu.kalnarapps.kalnardict.TranslationFragment.PRIMARY_LANG_ID;
import static eu.kalnarapps.kalnardict.TranslationFragment.SECONDARY_LANG_ID;

/**
 * Created by kalnar on 10/04/17.
 */

public class SliderFragment extends Fragment {

    @BindView(R.id.edit_text_field)
    protected EditText editText;
    @BindView(R.id.clearButton)
    protected Button button;
    @BindView(R.id.changeSearchDirectionButton)
    protected ImageButton changeSearchDirectionButton;
    @BindView(R.id.listView)
    protected ListView wordsListView;
    @BindView(R.id.favoritesButton)
    protected Button favoritesButton;
    @BindView(R.id.pullToRefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;
    private String langId = "1";
    private CursorAdapter adapter;
    private Cursor cursor;

    private String query = "SELECT id _id, word FROM %1$s_dictionary where langid=%2$s order by id limit 50"; // No trailing ';'
    private SQLiteDatabase sqLiteDatabase;
    private OnItemSelectedListener onItemSelectedListener;
    private DictionaryDbHelper dbHelper;
    private LanguagePair languagePair;
    private Mode mode = SEARCH;
    private boolean favoritesAscending = true;

    public SliderFragment() {
    }

    public static SliderFragment create(LanguagePair languagePair) {
        SliderFragment fragment = new SliderFragment();
        Bundle bundle = new Bundle();
        fragment.setLanguagePair(languagePair);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void setLanguagePair(LanguagePair languagePair) {
        this.languagePair = languagePair;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_layout, container, false);
        ButterKnife.bind(this, rootView);
        editText.setHint(LanguageService.getHint(languagePair, langId));
        installOnClickListenerOnButton();
        initiateListView();
        initiateEditText();
        return rootView;
    }

    private void initiateEditText() {
        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_GO) {
                Cursor c = ((CursorAdapter) (wordsListView.getAdapter())).getCursor();
                if (c.getCount() == 0) {
                    String text = editText.getText().toString();
                    editText.setText(text.substring(0, text.length() - 1));
                    editText.setSelection(text.length() - 1);
                    return true;
                }
                c.moveToPosition(0);
                onItemSelectedListener.onItemSelected(null, editText, 0, c.getInt(0));
                return true;
            }
            return false;
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });
        editText.setImeActionLabel("translate", EditorInfo.IME_ACTION_GO);
    }

    private void initiateListView() {
        dbHelper = DictionaryDbHelper.getInstance(getContext());
        sqLiteDatabase = dbHelper.getReadableDatabase();
//        cursor = sqLiteDatabase.rawQuery(String.format(query, language.getLangCode(), langId), null);
        cursor = dbHelper.getFavoritesCursor(languagePair, true);
        adapter = new CursorAdapter(getContext(), R.layout.listview_item_layout, cursor, 0);
        adapter.setFilterQueryProvider(constraint -> dbHelper.filter(constraint, languagePair, langId));
        wordsListView.setAdapter(adapter);
        wordsListView.setOnItemClickListener(this::itemSelected);
        swipeRefreshLayout.setDistanceToTriggerSync(1);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(false);
            focusEditTextAndShowKeyboard();
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        adapter.getFilter().filter(editText.getText().toString());
    }

    private void installOnClickListenerOnButton() {
        button.setOnClickListener(view -> focusEditTextAndShowKeyboardAndClearText());
        changeSearchDirectionButton.setOnClickListener(view -> changeLanguageDirection());
        favoritesButton.setOnClickListener(view -> {
            loadFavorites();
            favoritesAscending = !favoritesAscending;
        });
    }

    private void changeLanguageDirection() {
        langId = langId.equals(PRIMARY_LANG_ID) ? SECONDARY_LANG_ID : PRIMARY_LANG_ID;
        String message = LanguageService.getHint(languagePair, langId);
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        editText.setHint(LanguageService.getHint(languagePair, langId));
        adapter.getFilter().filter(editText.getText().toString());
    }

    protected void focusEditTextAndShowKeyboard() {
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    protected void focusEditTextAndShowKeyboardAndClearText() {
        editText.setText("");
        mode = SEARCH;
        focusEditTextAndShowKeyboard();
    }

    @Override
    public void onPause() {
        if (dbHelper != null) {
            dbHelper.close();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (dbHelper != null) {
            sqLiteDatabase = dbHelper.getReadableDatabase();
        }
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void itemSelected(AdapterView<?> parent, View view, int position, long id) {
        onItemSelectedListener.onItemSelected(parent, view, position, id);
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public String getLangId() {
        return langId;
    }

    public void loadFavorites() {
        mode = FAVORITES;
        if (dbHelper != null) {
            adapter.swapCursor(dbHelper.getFavoritesCursor(languagePair, favoritesAscending)).close();
        }
    }

    public void updateListView() {
        switch (mode) {
            case SEARCH:
                break;
            case FAVORITES:
                loadFavorites();
                break;
            default:
                break;
        }
    }

    enum Mode {
        SEARCH, FAVORITES
    }

    public interface OnItemSelectedListener {
        void onItemSelected(AdapterView<?> parent, View view, int position, long id);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the fragment's state
        setRetainInstance(true);
    }


}
