package eu.kalnarapps.kalnardict.gui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TableRow;
import android.widget.TextView;

import eu.kalnarapps.kalnardict.R;
import eu.kalnarapps.kalnardict.model.entity.LanguagePair;
import eu.kalnarapps.kalnardict.model.entity.LanguageService;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by kalnar on 11/04/17.
 */

public class LanguageChooserPopup extends PopupWindow {

    private List<OnLanguageSelectedListener> onLanguageSelectedListenerList = new ArrayList<OnLanguageSelectedListener>();

    public LanguageChooserPopup(View contentView, int width, int height) {
        super(contentView, width, height);
    }

    public static LanguageChooserPopup createAt(View view) {
        LayoutInflater layoutInflater = (LayoutInflater) view.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        LinearLayout popupView = (LinearLayout) layoutInflater.inflate(R.layout.popup_layout, null);
        buildLayoutFromLanguageList(popupView);
        LanguageChooserPopup popup = new LanguageChooserPopup(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        popup.setBackgroundDrawable(new ColorDrawable());
        popup.setOutsideTouchable(true);
        popup.showAsDropDown(view, 50, 0);
        popup.installOnClickListeners(popupView);
        return popup;
    }

    private static void buildLayoutFromLanguageList(LinearLayout popupView) {
        for (LanguagePair languagePair : LanguageService.getPairs(popupView.getContext())) {
            TextView languageView = new TextView(popupView.getContext());
            languageView.setText(languagePair.getId());
            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 0.4f);
            // TODO: move int values to constants or xml
            params.setMargins(15, 5, 5, 15);
            languageView.setLayoutParams(params);
            languageView.setTextSize(33);
            popupView.addView(languageView);
        }
    }


    private void installOnClickListeners(LinearLayout popupView) {
        ArrayList<LanguagePair> languagePairs = (ArrayList<LanguagePair>) LanguageService.getPairs(popupView.getContext());
        for (int i = 0; i < languagePairs.size(); i++) {
            int finalI = i;
            popupView.getChildAt(i).setOnClickListener(view -> notifyOnLanguageSelectedListeners(languagePairs.get(finalI)));
        }
    }

    public void notifyOnLanguageSelectedListeners(LanguagePair language) {
        for (OnLanguageSelectedListener onLanguageSelectedListener : onLanguageSelectedListenerList) {
            onLanguageSelectedListener.onLanguageSelected(language);
        }
        dismiss();
    }

    public void addOnLanguageSelectedListener(OnLanguageSelectedListener onLanguageSelectedListener) {
        onLanguageSelectedListenerList.add(onLanguageSelectedListener);
    }

    public interface OnLanguageSelectedListener {
        void onLanguageSelected(LanguagePair language);
    }
}
