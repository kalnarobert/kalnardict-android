package eu.kalnarapps.kalnardict.gui;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kalnar on 03/09/17.
 */

public class WebViewInterface {

    Context mContext;
    private List<EdgeReachedListener> edgeReachedListeners = new ArrayList<EdgeReachedListener>();
    private List<ButtonPressedListener> buttonPressedListeners = new ArrayList<ButtonPressedListener>();
    private FavoriteStateChecker favoriteStateChecker;

    public WebViewInterface(Context c) {
        mContext = c;
    }

    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }


    @JavascriptInterface
    public void log(String info) {
        Log.d("webinterface", info);
    }

    @JavascriptInterface
    public void toggleFavoriteState() {
        for (ButtonPressedListener buttonPressedListener : buttonPressedListeners) {
            buttonPressedListener.onButtonPressed(WebButton.FAVORITE);
        }
    }

    @JavascriptInterface
    public void onNextFavoriteButton() {
        for (ButtonPressedListener buttonPressedListener : buttonPressedListeners) {
            buttonPressedListener.onButtonPressed(WebButton.FAVORITE_NEXT);
        }
    }

    @JavascriptInterface
    public void onForvoButton() {
        for (ButtonPressedListener buttonPressedListener : buttonPressedListeners) {
            buttonPressedListener.onButtonPressed(WebButton.FORVO);
        }
    }

    @JavascriptInterface
    public void onPreviousFavoriteButton() {
        for (ButtonPressedListener buttonPressedListener : buttonPressedListeners) {
            buttonPressedListener.onButtonPressed(WebButton.FAVORITE_PREVIOUS);
        }
    }

    @JavascriptInterface
    public boolean isStateFavorite() {
        return (favoriteStateChecker == null) ? false : favoriteStateChecker.isStateFavorite();
    }

    @JavascriptInterface
    public void onReachedBottom() {
        for (EdgeReachedListener edgeReachedListener : edgeReachedListeners) {
            edgeReachedListener.onEdgeReached(Edge.BOTTOM);
        }
    }

    @JavascriptInterface
    public void onReachedTop() {
        for (EdgeReachedListener edgeReachedListener : edgeReachedListeners) {
            edgeReachedListener.onEdgeReached(Edge.TOP);
        }
    }

    @JavascriptInterface
    public void onSmallPage() {
        for (EdgeReachedListener edgeReachedListener : edgeReachedListeners) {
            edgeReachedListener.onEdgeReached(Edge.TOP_AND_BOTTOM);
        }
    }

    public void addEdgeReachedListener(EdgeReachedListener edgeReachedListener) {
        edgeReachedListeners.add(edgeReachedListener);
    }

    public void addButtonPressedListener(ButtonPressedListener buttonPressedListener) {
        buttonPressedListeners.add(buttonPressedListener);
    }

    public void setFavoriteStateChecker(FavoriteStateChecker favoriteStateChecker) {
        this.favoriteStateChecker = favoriteStateChecker;
    }

    public enum WebButton {
        FAVORITE, FAVORITE_NEXT, FORVO, FAVORITE_PREVIOUS
    }

    public enum Edge {
        BOTTOM, TOP, TOP_AND_BOTTOM
    }

    interface EdgeReachedListener {
        void onEdgeReached(Edge edge);
    }

    public interface ButtonPressedListener {
        void onButtonPressed(WebButton button);
    }

    public interface FavoriteStateChecker {
        boolean isStateFavorite();
    }
}
