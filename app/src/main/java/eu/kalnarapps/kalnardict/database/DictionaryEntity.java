package eu.kalnarapps.kalnardict.database;

/**
 * Created by kalnar on 17/08/17.
 */

public class DictionaryEntity {

    private static DictionaryEntity CURRENT = null;

    private int id;
    private String translation;
    private String word;

    public DictionaryEntity(int id, String word, String translation) {
        this.id = id;
        this.translation = translation;
        this.word = word;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    protected static void setCurrent(DictionaryEntity entity) {
        CURRENT = entity;
    }

    public static DictionaryEntity getCURRENT() {
        return CURRENT;
    }
}
