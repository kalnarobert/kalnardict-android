package eu.kalnarapps.kalnardict.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import eu.kalnarapps.kalnardict.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.kalnarapps.kalnardict.model.entity.LanguagePair;

/**
 * Created by kalnar on 12/04/17.
 */

public class DictionaryDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "kalnardict.db";
    private static final String COLUMN_FROM_ID = "fromId";
    private static final String COLUMN_TO_ID = "toId";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DatabaseInfo.DictionaryEntry.TABLE_NAME + " (" +
                    DatabaseInfo.DictionaryEntry._ID + " INTEGER PRIMARY KEY," +
                    DatabaseInfo.DictionaryEntry.COLUMN_NAME_TITLE + " TEXT," +
                    DatabaseInfo.DictionaryEntry.COLUMN_NAME_SUBTITLE + " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DatabaseInfo.DictionaryEntry.TABLE_NAME;
    private static final String FILE_DIR = "kalnarapps/kalnar-dict/database";
    private static final String FAV_TAG = "favorites-tag";
    private static DictionaryDbHelper INSTANCE;
    private final Context context;

    private DictionaryDbHelper(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        super(context, Environment.getExternalStorageDirectory() + File.separator + FILE_DIR + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        DictionaryDbHelper.INSTANCE = this;
    }

    public static DictionaryDbHelper getInstance(Context context) {
        if (INSTANCE == null) {
            return new DictionaryDbHelper(context);
        }
        return INSTANCE;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public DictionaryEntity getEntity(int id, LanguagePair languagePair) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(languagePair.getTableName(), new String[]{"id", "word", "translation"}, "id = ?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            DictionaryEntity entity = new DictionaryEntity(id, cursor.getString(1), cursor.getString(2));
            cursor.close();
            DictionaryEntity.setCurrent(entity);
            return entity;
        } else {
            return null;
        }
    }

    public DictionaryEntity getPreviousEntityFromFavorites(long id, LanguagePair languagePair) {
        if (!isEntityFavorite(id, languagePair)) {
            return null;
        }
        return getPreviousOrNextFavorite(getOrderIdOfFavorite(id, languagePair), languagePair, Step.PREVIOUS);
    }

    public DictionaryEntity getNextEntityFromFavorites(long id, LanguagePair languagePair) {
        Log.d(FAV_TAG, "next entity: id: " + id + ", languagePair; " + languagePair.getId());
        if (!isEntityFavorite(id, languagePair)) {
            return null;
        }
        return getPreviousOrNextFavorite(getOrderIdOfFavorite(id, languagePair), languagePair, Step.NEXT);
    }

    private Integer getOrderIdOfFavorite(long id, LanguagePair languagePair) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(languagePair.getFavoritesTableName(), new String[]{"orderId", "id"}, "id = ?", new String[]{String.valueOf(id)}, null, null, null);
        cursor.moveToFirst();
        int orderId = cursor.getInt(0);
        Log.d(FAV_TAG, "order id: " + orderId + ", id: " + id);
        cursor.close();
        return orderId;
    }


    private DictionaryEntity getPreviousOrNextFavorite(int orderId, LanguagePair languagePair, Step step) {
        if (orderId == 0) {
            return null;
        }
        SQLiteDatabase database = this.getReadableDatabase();
        String whereClause = step == Step.NEXT ? "orderId > ?" : "orderId < ?";
        String orderByClause = step == Step.NEXT ? "orderId" : "orderId desc";
        Cursor cursor = database.query(languagePair.getFavoritesTableName(), new String[]{"orderId", "id"}, whereClause, new String[]{String.valueOf(orderId)}, null, null, orderByClause, "1");

        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            int id = cursor.getInt(1);
            Log.d("fav", "id: " + id + ", orderid: " + cursor.getInt(0));
            cursor.close();
            return getEntity(id, languagePair);
        } else {
            return null;
        }
    }

    public Cursor getFavoritesCursor(LanguagePair languagePair, boolean ascending) {
        SQLiteDatabase database = this.getReadableDatabase();
        String orderByString = ascending ? "ASC" : "DESC";
        String query = "select dict.id _id, word, translation from " + languagePair.getTableName() + " dict join " + languagePair.getFavoritesTableName() + " fav on dict.id = fav.id order by fav.Orderid " + orderByString;
        Cursor cursor = database.rawQuery(query, null);
        return cursor;
    }

    public Cursor filter(CharSequence constraint, LanguagePair languagePair, String langId) {
        String firstOrFirstTwoLetter = null;
        final boolean emptyQuery = constraint.length() == 0;
        String query = "";
        if (emptyQuery) {
            query = "select rowid _id, letters, fromId, toId from %1$s_index where lang_id ='%2$s' and no_accent_id='%3$s' order by letters asc limit 1";
        } else {
            query = "select rowid _id, letters, fromId, toId from %1$s_index where Substr(letters ,1," + Math.max(1, Math.min(constraint.length(), 2)) + ")='%2$s' and lang_id ='%3$s' and no_accent_id='%4$s' order by letters asc limit 1";
            firstOrFirstTwoLetter = constraint.length() == 1 ? String.valueOf(constraint.charAt(0)) : (String) constraint.subSequence(0, 2);
        }
        final boolean seachingWithoutAccents = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getResources().getString(R.string.accent_search_key), false);
        final String accent_id = seachingWithoutAccents ? "1" : "0";
        Log.i("prferences", context.getResources().toString());
        String formattedQuery = "";
        if (emptyQuery) {
            formattedQuery = String.format(query, languagePair.getId(), langId, accent_id);
        } else {
            formattedQuery = String.format(query, languagePair.getId(), firstOrFirstTwoLetter, langId, accent_id);
        }
        Log.i("query", formattedQuery);
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(formattedQuery, null);
        cursor.moveToFirst();
        int fromId = cursor.getInt(cursor.getColumnIndex(COLUMN_FROM_ID));
        int toId = cursor.getInt(cursor.getColumnIndex(COLUMN_TO_ID));
        int toIdWhenEmptyQuery = fromId + 100;
        cursor.close();
        final String sql = "select id _id, word from %s_dictionary where " + (seachingWithoutAccents ? "word_without_accents" : "word") + " like";
        final String sqlSecondPart = " '" + constraint + "%'  and (" + (seachingWithoutAccents ? "no_accent_id" : "_id") + " between " + fromId + " and " + (emptyQuery ? toIdWhenEmptyQuery : toId) + ") and langid=" + langId + " limit 200";
        final String formattedSql = String.format(sql, languagePair.getId()) + sqlSecondPart;
        Log.i("formattedsql", formattedSql);
        return sqLiteDatabase.rawQuery(formattedSql, null);
    }

    public void toggleFavoriteState(long id, LanguagePair languagePair) {
        if (isEntityFavorite(id, languagePair)) {
            removeFavorite(id, languagePair);
        } else {
            addFavorite(id, languagePair);
        }
    }

    public List<String> listDictionaryTables() {
        String query = "select name from sqlite_master where type = 'table' and name like '%_dictionary'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        ArrayList<String> dictionaryTableList = new ArrayList<>();
        try (Cursor cursor = sqLiteDatabase.rawQuery(query, null)) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                // The Cursor is now set to the right position
                dictionaryTableList.add(cursor.getString(0).replace("_dictionary", ""));
            }
        }
        return dictionaryTableList;
    }

    private void addFavorite(long id, LanguagePair languagePair) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues myMap = new ContentValues();
        myMap.put("id", Long.toString(id));
        database.insert(languagePair.getFavoritesTableName(), null, myMap);
    }

    private void removeFavorite(long id, LanguagePair languagePair) {
        SQLiteDatabase database = this.getReadableDatabase();
        database.delete(languagePair.getFavoritesTableName(), "id=?", new String[]{Long.toString(id)});
    }

    public void removeFavorites(LanguagePair languagePair) {
        SQLiteDatabase database = this.getReadableDatabase();
        database.delete(languagePair.getFavoritesTableName(), null, null);
    }

    public boolean isEntityFavorite(long id, LanguagePair lang) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(lang.getFavoritesTableName(), new String[]{"orderId", "id"}, "id = ?", new String[]{String.valueOf(id)}, null, null, null);
        boolean isFavorite = (cursor != null && cursor.getCount() >= 1);
        cursor.close();
        return isFavorite;
    }

    public boolean isNotLastFavorite(long id, LanguagePair languagePair) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(languagePair.getFavoritesTableName(), new String[]{"orderId", "id"}, null, null, null, null, null);
        boolean isNotLast = (cursor != null && cursor.getCount() > 1);
        cursor.close();
        return isNotLast;
    }

    public String getLanguageCode(String tableName, int languageId) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(tableName, new String[]{"key", "value"}, "key = ?", new String[]{"language" + languageId}, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            String langCode = cursor.getString(1);
            cursor.close();
            return langCode;
        } else {
            cursor.close();
            return null;
        }
    }

    public String getLanguageCss(String tableName, int languageId) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(tableName, new String[]{"key", "value"}, "key = ?", new String[]{"language" + languageId + "css"}, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            String langCss = cursor.getString(1);
            cursor.close();
            return langCss;
        } else {
            cursor.close();
            return null;
        }
    }

    enum Step {
        NEXT, PREVIOUS
    }

}
