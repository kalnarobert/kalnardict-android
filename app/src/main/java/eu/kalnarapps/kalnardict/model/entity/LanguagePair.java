package eu.kalnarapps.kalnardict.model.entity;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import eu.kalnarapps.kalnardict.database.DictionaryDbHelper;

public class LanguagePair {

    public static final String CSS_DIR = "kalnarapps/kalnar-dict/css";
    private static final String TABLE_NAME_POSTFIX = "_dictionary";
    private static final String FAVORITES_TABLE_NAME_POSTFIX = "_favorites";
    private static final String FILE_TAG = "file_tag";
    private Language primaryLanguage;
    private Language secondaryLanguage;
    private String id;

    public LanguagePair(String pairId, Context context) {
        id = pairId;
        String table = id + "_meta";
        DictionaryDbHelper dbHelper = DictionaryDbHelper.getInstance(context);
        String primaryLanguageCode = dbHelper.getLanguageCode(table, 1);
        String primaryLanguageCss = dbHelper.getLanguageCss(table, 1);
        String secondaryLanguageCode = dbHelper.getLanguageCode(table, 2);
        String secondaryLanguageCss = null;
        if (secondaryLanguageCode != null) {
            secondaryLanguageCss = dbHelper.getLanguageCss(table, 1);
        }
        String languageCss1 = pairId + "_" + primaryLanguageCode + ".css";
        String languageCss2 = pairId + "_" + secondaryLanguageCode + ".css";
        //Environment.getExternalStorageDirectory() + File.separator + CSS_DIR + File.separator + DATABASE_NAME,
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            //handle case of no SDCARD present
        } else {
            String dir = Environment.getExternalStorageDirectory() + File.separator + CSS_DIR;
            //create folder
            File folder = new File(dir); //folder name
            folder.mkdirs();
            //create file
            for (String css : Arrays.asList(languageCss1, languageCss2)) {
                File file = new File(dir, css);
                try {
                    FileWriter out = new FileWriter(file);
                    out.write(primaryLanguageCss);
                    out.close();
                } catch (IOException e) {
                    Log.e(FILE_TAG, e.getLocalizedMessage());
                }
            }
        }
        primaryLanguage = new Language(primaryLanguageCode, languageCss1);
        secondaryLanguage = new Language(secondaryLanguageCode, languageCss2);
    }

    public String getId() {
        return id;
    }

    public Language getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(Language primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public Language getSecondaryLanguage() {
        return secondaryLanguage;
    }

    public void setSecondaryLanguage(Language secondaryLanguage) {
        this.secondaryLanguage = secondaryLanguage;
    }

    public String getTableName() {
        return id + TABLE_NAME_POSTFIX;
    }

    public String getFavoritesTableName() {
        return id + FAVORITES_TABLE_NAME_POSTFIX;
    }


}
//    ENGLISH(0, "en", "fromenglishtohungarian_webview.css", "fromhungariantoenglish_webview.css"),
//    SPANISH(3, "es", "fromspanishtohungarian_webview.css", "fromhungariantospanish_webview.css"),
//    ITALIEN(4, "it", "fromitalientohungarian_webview.css", "fromhungariantoitalien_webview.css"),
//    RUSSIAN(5, "ru", "fromrussiantohungarian_webview.css", "fromhungariantorussian_webview.css");
