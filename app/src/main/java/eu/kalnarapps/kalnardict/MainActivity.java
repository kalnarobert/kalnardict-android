package eu.kalnarapps.kalnardict;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import eu.kalnarapps.kalnardict.gui.LanguageChooserPopup;
import eu.kalnarapps.kalnardict.model.entity.LanguageService;

public class MainActivity extends FragmentActivity {
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL = 1234;
    private static Context context;
    private ViewPager pager;
    private SliderPagerAdapter pagerAdapter;

    public static Context getAppContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        requestReadExternalOrInitiatePager(null);
    }

    public void requestReadExternalOrInitiatePager(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL);
        } else {
            initiatePager();
        }
    }

    private void initiatePager() {
        setContentView(R.layout.activity_main);
        pagerAdapter = new SliderPagerAdapter(getSupportFragmentManager(), this);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        int lastLang = sharedPref.getInt(getString(R.string.last_language), 0);
        pager.setCurrentItem(lastLang);
    }

    public void openPreviews(View view) {
        LanguageChooserPopup popup = LanguageChooserPopup.createAt(view);
        popup.addOnLanguageSelectedListener(
                language -> pager.setCurrentItem(LanguageService.getPairs(getApplicationContext()).indexOf(language))
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isAllNecessqryPermissionsGranted()) {
            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(getString(R.string.last_language), pager.getCurrentItem());
            editor.commit();
        }
    }

    private boolean isAllNecessqryPermissionsGranted() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onBackPressed() {
        if (!isAllNecessqryPermissionsGranted()) {
            Log.i("back", "permission not granted");
            super.onBackPressed();
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        Fragment frag = pagerAdapter.getItem(pager.getCurrentItem());
        Log.w("fragment", "position: " + pagerAdapter.getItemPosition(frag));
        Log.w("fragment", "pager current item: " + pager.getCurrentItem());
        boolean poppedBackStack = false;
        for (Fragment fragment : fm.getFragments()) {
            if (fragment != null && fragment.isVisible()) {
                FragmentManager childFm = fragment.getChildFragmentManager();
                if (childFm.getBackStackEntryCount() > 0) {
                    childFm.popBackStack();
                    poppedBackStack = true;
                }
            }
        }
        if (poppedBackStack) {
            return;
        }

        super.onBackPressed();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initiatePager();
                } else {
                    setContentView(R.layout.permission_denied);
                }
                return;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public void showSettings(MenuItem item) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void showDeleteFavoritesActivity(MenuItem item) {
        Intent intent = new Intent(this, DeleteFavoritesActivity.class);
        startActivity(intent);
    }

    public void restartActivity(MenuItem item) {
        this.finish();
    }
}

