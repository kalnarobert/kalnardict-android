package eu.kalnarapps.kalnardict.database;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import eu.kalnarapps.kalnardict.R;

/**
 * Created by kalnar on 12/04/17.
 */

public class CursorAdapter extends ResourceCursorAdapter {

    public CursorAdapter(Context context, int layout, Cursor cursor, int flags) {
        super(context, layout, cursor, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name = view.findViewById(R.id.list_item_word);
        name.setText(cursor.getString(cursor.getColumnIndex(DatabaseInfo.DictionaryEntry.COLUMN_NAME_TITLE)));
    }
}
